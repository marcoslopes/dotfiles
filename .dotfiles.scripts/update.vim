#!/usr/bin/env bash

source "${HOME}/.dotfiles.resources/dotfiles.functions"

upgrade_vim_plug() {
    # Vim always seems to have a return of 1...
    vim -esu ~/.vimrc +PlugUpgrade +qa || true
}

update_vim_plugins() {
    # Vim always seems to have a return of 1...
    vim -esu ~/.vimrc +PlugClean! +PlugUpdate +qa || true
}

info "Upgrading vimplug... "
run upgrade_vim_plug

info "Updating vim plugins... "
run update_vim_plugins
