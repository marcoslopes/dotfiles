#!/usr/bin/env bash

# copied from https://github.com/justin8/dotfiles/blob/master/install

source "${HOME}/.dotfiles.resources/dotfiles.functions"

install_vim_plug() {
    info "Installing/updating vimplug... "
    if [[ ! -f ~/.vim/autoload/plug.vim ]]; then
        mkdir -p ~/.vim/autoload
        curl -sSfLo ~/.vim/autoload/plug.vim \
            https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
        info "Sucessfully installed vimplug"
    fi
}

install_vim_plug
source "${HOME}/.dotfiles.scripts/update.vim"
