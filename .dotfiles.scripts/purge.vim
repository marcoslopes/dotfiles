#!/usr/bin/env bash

source "${HOME}/.dotfiles.resources/dotfiles.functions"

info "Purging .vim*"

rm -rf ~/.vim*
