
# Source aliases
source ~/.dotfiles.resources/alias.dotfiles
source ~/.dotfiles.resources/alias.git
source ~/.dotfiles.resources/alias.misc
source ~/.dotfiles.resources/alias.expedia

# Source functions
# source ~/.functions

export PLATFORM_HOME="/Users/mlopes/workspace/expedia/ewe/teams/platform"
source ${PLATFORM_HOME}/.platform/functions

# configure linux binaries
GNUPATH="/usr/local/opt/coreutils/libexec/gnubin:/usr/local/opt/gnu-sed/libexec/gnubin"
export PATH="$GNUPATH:$PATH"


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
