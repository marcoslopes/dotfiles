"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set 7 lines to the cursor - when moving vertically using j/k
set so=10

" Turn on the WiLd menu
set wildmenu

" Ignore compiled files
set wildignore=*.o,*~,*.pyc

"Always show current position
set ruler

" When searching try to be smart about cases
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" Set line numbers
set number
set relativenumber

" Highlight search results
set hlsearch

" Clear search results on leader
nnoremap <leader><space> :noh<cr>

" Don't redraw while executing macros (good performance config)
set lazyredraw

" Show matching brackets when text indicator is over them
set showmatch

" highlight the line where the cursor is at for my poor eyes
set cursorline
hi CursorLine term=NONE cterm=NONE
" hi CursorLine term=NONE cterm=NONE guibg=Grey40 remove specific colour

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" show/hide the current mode
" set showmode
set noshowmode

" set nobackup
set noswapfile

" set all swap files to be in 
" set directory=~/.vim/swap

" Set visual characters, Textmate like
" set list
" set listchars=tab:▸\ ,eol:¬

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set termguicolors
" Enable syntax highlighting
if !exists("g:syntax_on")
    syntax enable
endif

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" enable 256-color mode.
set t_Co=256

" Use Unix as the standard file type
set ffs=unix,dos,mac

" this is set on the filetype plugin for python only
" highlights line if chars over 80
" highlight OverLength ctermbg=red ctermfg=white guibg=#592929
" match OverLength /\%81v.\+/

