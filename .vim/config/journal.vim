"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Diary, Log
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" <F2> - Insert a new log entry
" strftime insertion from http://vim.wikia.com/wiki/Insert_current_date_or_time
" map <f2> :$r! date +"\%Y-\%m-\%d \%H:\%M:\%S"<cr>
"imap <F2> <C-R>=strftime("%Y-%m-%d %H:%M:%S")<CR>
" nmap <F2> :$<CR>i<CR><C-R>=strftime("%A, %Y-%m-%d %H:%M:%S")<CR><Esc>o<C-R>
nnoremap <F2> Go<NL><Esc>i<Esc>"=strftime("\%A, \%Y-\%m-\%d \%H:\%M:\%S")<CR>po    - 
nnoremap <F3> Go<NL><NL><Esc>iWeek <Esc>"=strftime("\%V ")<CR>p<Esc>i goals:<CR>    - 

