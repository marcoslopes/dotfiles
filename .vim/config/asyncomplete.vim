" tab completion
" inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
" inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<cr>"

inoremap <expr> J pumvisible() ? "\<C-n>" : "j"
inoremap <expr> K pumvisible() ? "\<C-p>" : "k"
inoremap <expr> H pumvisible() ? "\<PageUp>\<C-n>\<C-p>" : "h"
inoremap <expr> L pumvisible() ? "\<PageDown>\<C-n>\<C-p>" : "l"



" force refresh completion
imap <c-space> <Plug>(asyncomplete_force_refresh)


"Auto popup
" By default asyncomplete will automatically show the autocomplete popup menu as you start typing.
" If you would like to disable the default behvior set g:asyncomplete_auto_popup to 0.
let g:asyncomplete_auto_popup = 0

" You can use the above <Plug>(asyncomplete_force_refresh) to show the popup or can you tab to show the autocomplete.
function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <TAB>
  \ pumvisible() ? "\<C-n>" :
  \ <SID>check_back_space() ? "\<TAB>" :
  \ asyncomplete#force_refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"


"Remove duplicates
"If you have many sources enabled (especially the buffer source), it might be useful to remove duplicates from the completion list.
"You can enable this by setting g:asyncomplete_remove_duplicates to 1.

let g:asyncomplete_remove_duplicates = 1

" Smart Completion
" To enable fuzzy smart completion:
let g:asyncomplete_smart_completion = 1
