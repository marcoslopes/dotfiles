" rust formatter: automatic running of :RustFmt when you save a buffer
let g:autofmt_autosave = 1

" show type information
let g:racer_experimental_completer = 1
