" https://www.reddit.com/r/rust/comments/6rx634/async_autocompletion_for_vim8_and_neovim_via_rls/

if executable('rls')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'rls',
        \ 'cmd': { server_info->['rustup', 'run', 'stable', 'rls'] },
        \ 'whitelist': ['rust'],
        \ })
endif

" let g:lsp_log_verbose = 1
" let g:lsp_log_file = expand('~/vim-lsp.log')
" 
" " for asyncomplete.vim log
" let g:asyncomplete_log_file = expand('~/asyncomplete.log')
