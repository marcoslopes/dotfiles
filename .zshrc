# zmodload zsh/zprof
#
# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# HomeBrew Path
HOMEBREWPATH="/usr/local/bin"

# GNU Binaries
GNUPATH="/usr/local/opt/coreutils/libexec/gnubin:/usr/local/opt/gnu-sed/libexec/gnubin"

# Setup path
export PATH="$HOMEBREWPATH:$GNUPATH:/usr/bin:/bin:/usr/sbin:/sbin"

# Add my own bin
export PATH="$PATH:$HOME/.bin:$HOME/.bin/shpotify"

# Man path
export MANPATH="/usr/local/share/man:$MANPATH"

# android sdk
# export ANDROID_HOME="/usr/local/opt/android-sdk"
# export PATH="$PATH:$ANDROID_HOME/bin"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi
#
export EDITOR=vim
export VISUAL=vim

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# do not ad these to my history
export HISTIGNORE='clear:history'
# ignore duplicate commands(ignoredups)
# ignore leading spaced commands (ignorespace)
export HISTCONTROL='ignoreboth'

export GIT_PS1_SHOWDIRTYSTATE=true

# Source aliases
source ~/.dotfiles.resources/alias.dotfiles
source ~/.dotfiles.resources/alias.git
source ~/.dotfiles.resources/alias.misc

# Source functions
source ~/.dotfiles.resources/functions.misc

# Source Development Configuration
source ~/.dotfiles.resources/dev.zshrc

# set vim mode
bindkey -v

# use question mark for history backwards incremental search
bindkey -M vicmd '?' history-incremental-search-backward

# ctrl j/k for up and down
bindkey '^K' up-line-or-search
bindkey '^J' down-line-or-search
# zprof
