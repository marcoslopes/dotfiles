" Combination of these setting plus others
" http://amix.dk/vim/vimrc.html
" https://github.com/jaysw/dotfiles/blob/master/vimrc

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => https://github.com/junegunn/vim-plug 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
filetype off " required
call plug#begin('~/.vim/plugged')

Plug 'itchyny/lightline.vim'
Plug 'mengelbrecht/lightline-bufferline'

Plug 'kien/ctrlp.vim'
Plug 'mhinz/vim-signify'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'Raimondi/delimitMate'
Plug 'scrooloose/nerdtree', { 'on': ['NERDTreeToggle', 'NERDTreeFind'] }
Plug 'hashivim/vim-terraform', { 'for': [ 'terraform' ] }
Plug 'davidhalter/jedi-vim', { 'for': [ 'python' ] }
Plug 'nvie/vim-flake8', { 'for': [ 'python' ] }
Plug 'chrisbra/csv.vim', { 'for': [ 'csv' ] }
Plug 'cespare/vim-toml', { 'for': [ 'toml' ] }
Plug 'ekalinin/Dockerfile.vim', { 'for': [ 'Dockerfile' ] }
Plug 'mattn/emmet-vim', { 'for': [ 'html', 'mustache', 'xml' ] }
Plug 'rust-lang/rust.vim', { 'for': [ 'rust' ] }
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'tweekmonster/startuptime.vim'
" Plug 'w0rp/ale'
" Plug 'SirVer/ultisnips'
" Plug 'honza/vim-snippets'
" Plug 'ervandew/supertab'
" Plug 'airblade/vim-rooter'
" Plug 'mileszs/ack.vim'
" Plug 'majutsushi/tagbar', { 'on': ['Tagbar'] }
" Plug 'rodjek/vim-puppet', { 'for': [ 'puppet' ] }
" Plug 'wavded/vim-stylus', { 'for': [ 'styl' ] }
" Plug 'roxma/nvim-yarp', { 'for': [ 'rust' ]}
" Plug 'roxma/vim-hug-neovim-rpc', { 'for': [ 'rust' ]}
" Plug 'valloric/YouCompleteMe', { 'for': [ 'rust' ] }
" Plug 'airblade/vim-gitgutter' signify should cater for perforce, but this
" might be faster, check it out

" colourschemes
Plug 'arcticicestudio/nord-vim'
" Plug 'MarcWeber/vim-addon-local-vimrc'
" Multiple file types
" Plug 'kovisoft/paredit', { 'for': ['clojure', 'scheme'] }
call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Basic Setup
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" get rid of Vi compatibility mode. SET FIRST!
set nocompatible

" http://lists.alioth.debian.org/pipermail/pkg-vim-maintainers/2007-June/004020.html
set modelines=0

" Map the leader key to comma
let mapleader=","

" Use semicolon as colon
nnoremap ; :

" Esc shall be jj
inoremap jk <ESC>

" show intermediate commands
set showcmd

" allow registers to be used with clipboard
set clipboard=unnamed

" let <leader> cf to copy file path
nnoremap <leader>cf :let @*=@%<cr>

" Yank consistent with C D S
nnoremap Y y$

" filetype [ON] plugin [ON] ident [ON]
filetype plugin indent on

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4
set softtabstop=4

" always indent/outdent to the nearest tabstop
set shiftround

" whitespace fixes
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Editing mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remap VIM 0 to first non-blank character
map 0 ^

" allow for a longer default mapping combination timeout
set timeoutlen=1500

" Move a line of text using ALT+[jk] or Comamnd+[jk] on mac
" nmap <M-j> mz:m+<cr>`z
" nmap <M-k> mz:m-2<cr>`z
" vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
" vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

if has("mac") || has("macunix")
  nmap <D-j> <M-j>
  nmap <D-k> <M-k>
  vmap <D-j> <M-j>
  vmap <D-k> <M-k>
endif

" Delete trailing white space on save, useful for Python and CoffeeScript ;)
func! DeleteTrailingWS()
  exe "normal mz"
  %s/\s\+$//ge
  exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()
autocmd BufWrite *.coffee :call DeleteTrailingWS()

" Make < > shifts keep selection
vnoremap < <gv
vnoremap > >gv

" Toggle paste
nmap <leader>p :set paste!<CR>

" select pasted text
nnoremap <leader>v V`]
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Moving around, tabs, windows and buffers
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set mouse for scrolling
set mouse=a

" Tab for moving bracket pairs
nnoremap <tab> %
vnoremap <tab> %

" Treat long lines as break lines (useful when moving around in them)
map j gj
map k gk

" unmap arrow keys ftw
no <down> <Nop>
no <left> <Nop>
no <right> <Nop>
no <up> <Nop>

ino <down> <Nop>
ino <left> <Nop>
ino <right> <Nop>
ino <up> <Nop>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Search
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" fix Vim’s horribly broken default regex “handling” by automatically
" inserting a \v before any string you search for.
" nnoremap / /\v
" vnoremap / /\v

" Custom highlighting of dates in log files. Adapted from
" http://stackoverflow.com/questions/4167425/custom-syntax-highlighting-in-vim
au BufRead,BufNewFile *.log hi logheader guifg=navy ctermfg=blue term=bold cterm=bold gui=bold
" Matches lines starting with @ and ending in M (see log entry format below)
au BufRead,BufNewFile *.log syn match logheader /^@.*M$/

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins are found under .vim/config/pluginname.vim


" sets history after everything else
set history=10000

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colorsscheme
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" colorscheme two-firewatch
colorscheme nord
set background=dark
" let g:solarized_termcolors=256

runtime! config/**/*.vim
