# Set Java Home
export JAVA_HOME="/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home"
export PATH="$JAVA_HOME/bin:$PATH"

# Increase Maven heap
export MAVEN_OPTS="-Xmx512m $MAVEN_OPTS"
# always use maven with the specified JVM
export MAVEN_SKIP_RC="skip"

# Eclim
export PATH="$PATH:/Applications/Eclipse Java.app/Contents/Eclipse"

# Local Python
export PATH="$PATH:$HOME/Library/Python/2.7/bin"

# Set Go Home
GOROOT="/usr/local/Cellar/go/1.6/"
# export PATH="$PATH:$GOROOT/bin"
# export PATH="$PATH:$GOPATH/bin"

export P4CONFIG=p4.ini
export P4EDITOR=vim

# Ruby shit
export RBENV_ROOT=/usr/local/var/rbenv
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

# Rust
export PATH="$HOME/.cargo/bin:$PATH"
export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"

source ~/.dotfiles.resources/alias.expedia
source ~/.dotfiles.resources/functions.expedia

export PLATFORM_HOME="/Users/mlopes/workspace/expedia/platform"
source ${PLATFORM_HOME}/.platform/functions

# Ruby shit,  this makes the loadint very very very slow
# eval "$(rbenv init -)"
#
# Node JS shit
# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
# [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
